A simple alias to [bpytop](https://github.com/aristocratos/bpytop).

# <img src="https://cdn.rawgit.com/oh-my-fish/oh-my-fish/e4f1c2e0219a17e2c748b824004c8d0b38055c16/docs/logo.svg" width="28px" height="28px"/> htop2bpytop

A plugin for [Oh My Fish][oh-my-fish]. 

[![MIT License](https://img.shields.io/badge/License-MIT-blue?style=for-the-badge)](LICENSE.md)
[![Fish Shell](https://img.shields.io/badge/fish-3.2.2-blue?style=for-the-badge)](https://fishshell.com)
[![Oh My Fish](https://img.shields.io/badge/Oh%20My%20Fish-Fishshell--Framework-blue?style=for-the-badge)](https://github.com/oh-my-fish/oh-my-fish)

Like `htop`, but on steroids.

## Install

**bpytop** needs to be installed first for this to work! Visit [the installation section on the GitHub repository](https://github.com/aristocratos/bpytop#installation)!

Once `bpytop` is installed, you can install the _alias_:

```shell script
$ omf install htop2bpytop
```

## Usage

Just use `htop` as usual.

# License

[MIT][mit] © [Marc-André Appel][author]

[oh-my-fish]: https://www.github.com/oh-my-fish/oh-my-fish
[author]: https://gitlab.com/marc-andre
[mit]: /LICENSE.md
